mod scanner;
use scanner::Scanner;

mod char_consts;
use char_consts::*;

#[cfg(test)]
mod tests;

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum ECMAParseError {
    UnexpectedEOF,
    UnexpectedChar,
    ExpectedCommonToken,
    InvalidCodePoint,
    NoHexDigit,
    SyntaxError,
}

#[allow(non_camel_case_types)]
#[derive(PartialEq, Debug, Clone, Copy)]
pub enum TokenType {
    WhiteSpace,
    LineTerminator,
    LineTerminatorSequence,
    SourceCharacter,
    Comment,
    MultiLineComment,
    MultiLineCommentChars,
    PostAsteriskCommentChars,
    MultiLineNotAsteriskChar,
    MultiLineNotForwardSlashOrAsteriskChar,
    SingleLineComment,
    SingleLineCommentChars,
    SingleLineCommentChar,
    CommonToken,
    PrivateIdentifier,
    IdentifierName,
    IdentifierStart,
    IdentifierPart,
    IdentifierStartChar,
    IdentifierPartChar,
    UnicodeIDStart,
    UnicodeIDContinue,
    UnicodeEscapeSequence,
    Hex4Digits,
    HexDigit,
    HexDigits,
    HexDigits_Sep,
    DecimalDigit,
    DecimalDigits_Sep,
    NumericLiteralSeparator,
    CodePoint,
    Punctuator,
    OptionalChainingPunctuator,
    OtherPunctuator,
    DivPunctuator,
    RightBracePunctuator,
    NullLiteral,
    BooleanLiteral,
    NumericLiteral,
    DecimalBigIntegerLiteral,
    NonDecimalIntegerLiteral,
    NonDecimalIntegerLiteral_Sep,
    BigIntLiteralSuffix,
    DecimalLiteral,
    DecimalIntegerLiteral,
    DecimalDigits,
    NonZeroDigit,
    ExponentPart,
    ExponentPart_Sep,
    ExponentIndicator,
    SignedInteger,
    SignedInteger_Sep,
    BinaryIntegerLiteral,
    BinaryIntegerLiteral_Sep,
    BinaryDigits,
    BinaryDigits_Sep,
    BinaryDigit,
    OctalIntegerLiteral,
    OctalIntegerLiteral_Sep,
    OctalDigits,
    OctalDigits_Sep,
    LegacyOctalIntegerLiteral,
    NonOctalDecimalIntegerLiteral,
    LegacyOctalLikeDecimalIntegerLiteral,
    OctalDigit,
    NonOctalDigit,
    HexIntegerLiteral,
    HexIntegerLiteral_Sep,
    StringLiteral,
    SingleEscapeCharacter,
    NonZeroOctalDigit,
    ZeroToThree,
    FourToSeven,
    NonOctalDecimalEscapeSequence,
    LegacyOctalEscapeSequence,
    LineContinuation,
    NonEscapeCharacter,
    EscapeCharacter,
    CharacterEscapeSequence,
    HexEscapeSequence,
    EscapeSequence,
    DoubleStringCharacter,
    SingleStringCharacter,
    DoubleStringCharacters,
    SingleStringCharacters,
    Template,
    ReservedWord,
}

#[derive(Debug, PartialEq)]
pub struct Token {
    pub ttype: TokenType,
    pub loc: usize,
    pub children: Vec<Token>,
}

use ECMAParseError::*;
use TokenType::*;

impl Token {
    pub fn parse_or_rewind(
        ttype: TokenType,
        scanner: &mut Scanner,
        source: &str,
    ) -> Result<Token, ECMAParseError> {
        let loc = scanner.cursor();
        let res = Token::parse(ttype, scanner, source);
        if res.is_err() {
            scanner.set_cursor(loc);
        }
        res
    }

    pub fn parse(
        ttype: TokenType,
        scanner: &mut Scanner,
        source: &str,
    ) -> Result<Token, ECMAParseError> {
        let loc = scanner.cursor();
        match ttype {
            // WhiteSpace ::
            // 	<TAB>
            // 	<VT>
            // 	<FF>
            // 	<ZWNBSP>
            // 	<USP>
            WhiteSpace => match scanner.peek() {
                Some(&TAB) | Some(&VT) | Some(&FF) | Some(&ZWNBSP) => {
                    scanner.pop();
                    Ok(Token {
                        ttype: WhiteSpace,
                        loc,
                        children: vec![],
                    })
                }
                Some(&c)
                    if unicode_general_category::get_general_category(c)
                        == unicode_general_category::GeneralCategory::SpaceSeparator =>
                {
                    scanner.pop();
                    Ok(Token {
                        ttype: WhiteSpace,
                        loc,
                        children: vec![],
                    })
                }
                Some(_) => Err(UnexpectedChar),
                None => Err(UnexpectedEOF),
            },
            // LineTerminator ::
            // 	<LF>
            // 	<CR>
            // 	<LS>
            // 	<PS>
            LineTerminator => {
                if scanner.take(&LF) || scanner.take(&CR) || scanner.take(&LS) || scanner.take(&PS)
                {
                    Ok(Token {
                        ttype: LineTerminator,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(if scanner.is_done() {
                        UnexpectedEOF
                    } else {
                        UnexpectedChar
                    })
                }
            }
            // LineTerminatorSequence ::
            // 	<LF>
            // 	<CR> [lookahead ≠ <LF>]
            // 	<LS>
            // 	<PS>
            // 	<CR> <LF>
            LineTerminatorSequence => {
                if scanner.take(&LF) || scanner.take(&LS) || scanner.take(&PS) {
                    Ok(Token {
                        ttype: LineTerminatorSequence,
                        loc,
                        children: vec![],
                    })
                } else if scanner.take(&CR) {
                    scanner.take(&LF);
                    Ok(Token {
                        ttype: LineTerminatorSequence,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(if scanner.is_done() {
                        UnexpectedEOF
                    } else {
                        UnexpectedChar
                    })
                }
            }
            SourceCharacter => {
                if scanner.is_done() {
                    Err(UnexpectedEOF)
                } else {
                    scanner.pop();
                    Ok(Token {
                        ttype: SourceCharacter,
                        loc,
                        children: vec![],
                    })
                }
            }
            // SingleLineCommentChar ::
            // 	SourceCharacter but not LineTerminator
            SingleLineCommentChar => {
                let line_terminator = Token::parse_or_rewind(LineTerminator, scanner, source);
                if let Err(UnexpectedChar) = line_terminator {
                    Ok(Token {
                        ttype: SingleLineCommentChar,
                        loc,
                        children: vec![
                            Token::parse_or_rewind(SourceCharacter, scanner, source).unwrap()
                        ],
                    })
                } else if let Err(UnexpectedEOF) = line_terminator {
                    Err(UnexpectedEOF)
                } else {
                    Err(UnexpectedChar)
                }
            }
            // SingleLineCommentChars ::
            // 	SingleLineCommentChar SingleLineCommentChars_{opt}
            SingleLineCommentChars => {
                let mut children = vec![Token::parse_or_rewind(
                    SingleLineCommentChar,
                    scanner,
                    source,
                )?];
                while let Ok(t) = Token::parse_or_rewind(SingleLineCommentChar, scanner, source) {
                    children.push(t)
                }
                Ok(Token {
                    ttype: SingleLineCommentChars,
                    loc,
                    children,
                })
            }
            // SingleLineComment ::
            // 	// SingleLineCommentChars_{opt}
            SingleLineComment => {
                if scanner.take(&'/') && scanner.take(&'/') {
                    let mut children = vec![];
                    if let Ok(t) = Token::parse_or_rewind(SingleLineCommentChars, scanner, source) {
                        children.push(t);
                    }
                    Ok(Token {
                        ttype: SingleLineComment,
                        loc,
                        children,
                    })
                } else if scanner.is_done() {
                    Err(UnexpectedEOF)
                } else {
                    Err(UnexpectedChar)
                }
            }
            // Comment ::
            // 	MultiLineComment
            // 	SingleLineComment
            Comment => Ok(Token {
                ttype: Comment,
                loc,
                children: if let Ok(t) = Token::parse_or_rewind(MultiLineComment, scanner, source) {
                    vec![t]
                } else if let Ok(t) = Token::parse_or_rewind(SingleLineComment, scanner, source) {
                    vec![t]
                } else {
                    return Err(if scanner.is_done() {
                        UnexpectedEOF
                    } else {
                        UnexpectedChar
                    });
                },
            }),
            // MultiLineComment ::
            // 	/* MultiLineCommentChars_{opt} */
            MultiLineComment => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take(&'/') {
                    return Err(UnexpectedChar);
                }
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take(&'*') {
                    return Err(UnexpectedChar);
                }
                let mut children = vec![];
                if let Ok(t) = Token::parse_or_rewind(MultiLineCommentChars, scanner, source) {
                    // println!("{t:#?}");
                    children.push(t);
                }
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take(&'*') {
                    return Err(UnexpectedChar);
                }
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take(&'/') {
                    return Err(UnexpectedChar);
                }
                Ok(Token {
                    ttype: MultiLineComment,
                    loc,
                    children,
                })
            }
            // MultiLineCommentChars ::
            // 	MultiLineNotAsteriskChar MultiLineCommentChars_{opt}
            // 	* PostAsteriskCommentChars_{opt}
            MultiLineCommentChars => {
                if let Ok(t) = Token::parse_or_rewind(MultiLineNotAsteriskChar, scanner, source) {
                    let mut children = vec![t];
                    if let Ok(t) = Token::parse_or_rewind(MultiLineCommentChars, scanner, source) {
                        children.push(t)
                    }
                    Ok(Token {
                        ttype: MultiLineCommentChars,
                        loc,
                        children,
                    })
                } else if scanner.take(&'*') {
                    // this is technicaly not conformant with the specs,
                    // but i wasn't able to get the spec-conforming version to work
                    if scanner.peek() == Some(&'/') {
                        Err(UnexpectedChar)
                    } else {
                        let mut children = vec![];
                        if let Ok(t) =
                            Token::parse_or_rewind(PostAsteriskCommentChars, scanner, source)
                        {
                            children.push(t);
                        }
                        Ok(Token {
                            ttype: MultiLineCommentChars,
                            loc,
                            children,
                        })
                    }
                } else {
                    Err(if scanner.is_done() {
                        UnexpectedEOF
                    } else {
                        UnexpectedChar
                    })
                }
            }
            // PostAsteriskCommentChars ::
            // 	MultiLineNotForwardSlashOrAsteriskChar MultiLineCommentChars_{opt}
            // 	* PostAsteriskCommentChars_{opt}
            PostAsteriskCommentChars => {
                if let Ok(t) =
                    Token::parse_or_rewind(MultiLineNotForwardSlashOrAsteriskChar, scanner, source)
                {
                    let mut children = vec![t];
                    if let Ok(t) = Token::parse_or_rewind(MultiLineCommentChars, scanner, source) {
                        children.push(t)
                    }
                    Ok(Token {
                        ttype: PostAsteriskCommentChars,
                        loc,
                        children,
                    })
                //											not spec compliant
                //											(but it works_tm)
                //													|
                //													v
                //									|-----------------------------|
                //									|							  |
                } else if scanner.take(&'*') && scanner.peek() != Some(&'/') {
                    let mut children = vec![];
                    if let Ok(t) = Token::parse_or_rewind(PostAsteriskCommentChars, scanner, source)
                    {
                        children.push(t)
                    }
                    Ok(Token {
                        ttype: PostAsteriskCommentChars,
                        loc,
                        children,
                    })
                } else {
                    Err(if scanner.is_done() {
                        UnexpectedEOF
                    } else {
                        UnexpectedChar
                    })
                }
            }
            // MultiLineNotAsteriskChar ::
            // 	SourceCharacter but not *
            MultiLineNotAsteriskChar => {
                if scanner.peek() == Some(&'*') {
                    Err(UnexpectedChar)
                } else {
                    Ok(Token {
                        ttype: MultiLineNotForwardSlashOrAsteriskChar,
                        loc,
                        children: vec![Token::parse(SourceCharacter, scanner, source)?],
                    })
                }
            }
            // MultiLineNotForwardSlashOrAsteriskChar ::
            // 	SourceCharacter but not one of / or *
            MultiLineNotForwardSlashOrAsteriskChar => {
                if scanner.peek() == Some(&'/') || scanner.peek() == Some(&'*') {
                    Err(UnexpectedChar)
                } else {
                    Ok(Token {
                        ttype: MultiLineNotForwardSlashOrAsteriskChar,
                        loc,
                        children: vec![Token::parse(SourceCharacter, scanner, source)?],
                    })
                }
            }
            CommonToken => {
                for sub_t in [
                    IdentifierName,
                    PrivateIdentifier,
                    Punctuator,
                    NumericLiteral,
                    StringLiteral,
                    Template,
                ] {
                    if let Ok(tok) = Token::parse_or_rewind(sub_t, scanner, source) {
                        return Ok(tok);
                    }
                }
                Err(ExpectedCommonToken)
            }
            // PrivateIdentifier ::
            // 	# IdentifierName
            PrivateIdentifier => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take(&'#') {
                    return Err(UnexpectedChar);
                }
                Ok(Token {
                    ttype: PrivateIdentifier,
                    loc,
                    children: vec![Token::parse_or_rewind(IdentifierName, scanner, source)?],
                })
            }
            // IdentifierName ::
            // 	IdentifierStart
            // 	IdentifierName IdentifierPart
            IdentifierName => {
                let mut children = vec![Token::parse_or_rewind(IdentifierStart, scanner, source)?];
                while let Ok(part) = Token::parse_or_rewind(IdentifierPart, scanner, source) {
                    children.push(part);
                }
                Ok(Token {
                    ttype: IdentifierName,
                    loc,
                    children,
                })
            }
            // IdentifierStart ::
            // 	IdentifierStartChar
            // 	\ UnicodeEscapeSequence
            IdentifierStart => {
                if let Ok(tok) = Token::parse_or_rewind(IdentifierStartChar, scanner, source) {
                    Ok(Token {
                        ttype: IdentifierStart,
                        loc,
                        children: vec![tok],
                    })
                } else if scanner.take(&'\\') {
                    let escape_sequence =
                        Token::parse_or_rewind(UnicodeEscapeSequence, scanner, source)?;
                    let code_point = escape_sequence.identifier_code_point(source);
                    if code_point != '_'
                        && code_point != '$'
                        && !unicode_id_start::is_id_start(code_point)
                    {
                        return Err(SyntaxError);
                    }
                    Ok(Token {
                        ttype: IdentifierStart,
                        loc,
                        children: vec![escape_sequence],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // IdentifierPart ::
            // 	IdentifierPartChar
            // 	\ UnicodeEscapeSequence
            IdentifierPart => {
                if let Ok(tok) = Token::parse_or_rewind(IdentifierPartChar, scanner, source) {
                    Ok(Token {
                        ttype: IdentifierPart,
                        loc,
                        children: vec![tok],
                    })
                } else if scanner.take(&'\\') {
                    let escape_sequence =
                        Token::parse_or_rewind(UnicodeEscapeSequence, scanner, source)?;
                    let code_point = escape_sequence.identifier_code_point(source);
                    if code_point != '$'
                        && code_point != ZWNJ
                        && code_point != ZWJ
                        && !unicode_id_start::is_id_continue(code_point)
                    {
                        return Err(SyntaxError);
                    }
                    Ok(Token {
                        ttype: IdentifierPart,
                        loc,
                        children: vec![escape_sequence],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // IdentifierStartChar ::
            // 	UnicodeIDStart
            // 	$
            // 	_
            IdentifierStartChar => {
                if scanner.take(&'$') || scanner.take(&'_') {
                    return Ok(Token {
                        ttype: IdentifierStartChar,
                        loc,
                        children: vec![],
                    });
                }
                Ok(Token {
                    ttype: IdentifierStartChar,
                    loc,
                    children: vec![Token::parse_or_rewind(UnicodeIDStart, scanner, source)?],
                })
            }
            // IdentifierPartChar ::
            // 	UnicodeIDContinue
            // 	$
            // 	<ZWNJ>
            // 	<ZWJ>
            IdentifierPartChar => {
                if scanner.take(&'$') || scanner.take(&ZWNJ) || scanner.take(&ZWJ) {
                    return Ok(Token {
                        ttype: IdentifierPartChar,
                        loc,
                        children: vec![],
                    });
                }
                Ok(Token {
                    ttype: IdentifierPartChar,
                    loc,
                    children: vec![Token::parse_or_rewind(UnicodeIDContinue, scanner, source)?],
                })
            }
            // UnicodeIDStart ::
            // 	any Unicode code point with the Unicode property “ID_Start”
            UnicodeIDStart => match scanner.peek() {
                Some(&ch) if unicode_id_start::is_id_start(ch) => {
                    scanner.pop();
                    Ok(Token {
                        ttype: UnicodeIDStart,
                        loc,
                        children: vec![],
                    })
                }
                Some(_) => Err(UnexpectedChar),
                None => Err(UnexpectedEOF),
            },
            // UnicodeIDContinue ::
            // 	any Unicode code point with the Unicode property “ID_Continue”
            UnicodeIDContinue => match scanner.peek() {
                Some(&ch) if unicode_id_start::is_id_continue(ch) => {
                    scanner.pop();
                    Ok(Token {
                        ttype: UnicodeIDContinue,
                        loc,
                        children: vec![],
                    })
                }
                Some(_) => Err(UnexpectedChar),
                None => Err(UnexpectedEOF),
            },
            // UnicodeEscapeSequence ::
            // 	u Hex4Digits
            // 	u{ CodePoint }
            UnicodeEscapeSequence => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take(&'u') {
                    return Err(UnexpectedChar);
                }
                let dig = Token::parse_or_rewind(Hex4Digits, scanner, source);
                if let Ok(tok) = dig {
                    Ok(Token {
                        ttype: UnicodeEscapeSequence,
                        loc,
                        children: vec![tok],
                    })
                } else if dig == Err(UnexpectedChar) {
                    if scanner.take(&'{') {
                        let cpt = Token::parse_or_rewind(CodePoint, scanner, source)?;
                        if !scanner.take(&'}') {
                            return Err(UnexpectedChar);
                        }
                        Ok(Token {
                            ttype: UnicodeEscapeSequence,
                            loc,
                            children: vec![cpt],
                        })
                    } else {
                        Err(UnexpectedChar)
                    }
                } else {
                    dig
                }
            }
            // Hex4Digits ::
            // 	HexDigit HexDigit HexDigit HexDigit
            Hex4Digits => Ok(Token {
                ttype: Hex4Digits,
                loc,
                children: vec![
                    Token::parse_or_rewind(HexDigit, scanner, source)?,
                    Token::parse_or_rewind(HexDigit, scanner, source)?,
                    Token::parse_or_rewind(HexDigit, scanner, source)?,
                    Token::parse_or_rewind(HexDigit, scanner, source)?,
                ],
            }),
            // HexDigit :: one of
            // 	0 1 2 3 4 5 6 7 8 9 a b c d e f A B C D E F
            HexDigit => match scanner.peek() {
                Some(c)
                    if [
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
                        'f', 'A', 'B', 'C', 'D', 'E', 'F',
                    ]
                    .contains(c) =>
                {
                    scanner.pop();
                    Ok(Token {
                        ttype: HexDigit,
                        loc,
                        children: vec![],
                    })
                }
                Some(_) => Err(UnexpectedChar),
                None => Err(UnexpectedEOF),
            },
            // CodePoint ::
            // 	HexDigits_{[~Sep]} but only if MV of HexDigits ≤ 0x10FFFF
            CodePoint => {
                let dig = Token::parse_or_rewind(HexDigits, scanner, source)?;
                if dig.mv(source) > 0x10FFFF as f64 {
                    return Err(InvalidCodePoint);
                }
                Ok(Token {
                    ttype: CodePoint,
                    loc,
                    children: vec![dig],
                })
            }
            // HexDigits_{[Sep]} ::
            // 	HexDigit
            // 	HexDigits_{[?Sep]} HexDigit
            // 	[+Sep] HexDigits_{[+Sep]} NumericLiteralSeparator HexDigit
            HexDigits | HexDigits_Sep => {
                let mut children = vec![];
                while let Ok(tok) = Token::parse_or_rewind(HexDigit, scanner, source) {
                    children.push(tok);
                    if ttype == HexDigits_Sep {
                        if let Ok(sep) =
                            Token::parse_or_rewind(NumericLiteralSeparator, scanner, source)
                        {
                            children.push(sep);
                        }
                    }
                }
                if children.len() == 0 {
                    Err(NoHexDigit)
                } else {
                    Ok(Token {
                        ttype: ttype,
                        loc,
                        children,
                    })
                }
            }
            // NumericLiteralSeparator ::
            // 	_
            NumericLiteralSeparator => match scanner.peek() {
                Some(&'_') => {
                    scanner.pop();
                    Ok(Token {
                        ttype: NumericLiteralSeparator,
                        loc,
                        children: vec![],
                    })
                }
                Some(_) => Err(UnexpectedChar),
                None => Err(UnexpectedEOF),
            },
            // ReservedWord :: one of
            // 	await break case catch class const continue debugger default delete
            // 	do else enum export extends false finally for function if import in
            // 	instanceof new null return super switch this throw true try typeof
            // 	var void while with yield
            ReservedWord => {
                const WORDS: &[&str; 38] = &[
                    "await",
                    "break",
                    "case",
                    "catch",
                    "class",
                    "const",
                    "continue",
                    "debugger",
                    "default",
                    "delete",
                    "do",
                    "else",
                    "enum",
                    "export",
                    "extends",
                    "false",
                    "finally",
                    "for",
                    "function",
                    "if",
                    "import",
                    "in",
                    "instanceof",
                    "new",
                    "null",
                    "return",
                    "super",
                    "switch",
                    "this",
                    "throw",
                    "true",
                    "try",
                    "typeof",
                    "var",
                    "void",
                    "while",
                    "with",
                    "yield",
                ];
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                for &w in WORDS {
                    if scanner.take_str(w) {
                        return Ok(Token {
                            ttype: ReservedWord,
                            loc,
                            children: vec![],
                        });
                    }
                }
                Err(UnexpectedChar)
            }
            // Punctuator ::
            // 	OptionalChainingPunctuator
            // 	OtherPunctuator
            Punctuator => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                let tok = if let Ok(tok) =
                    Token::parse_or_rewind(OptionalChainingPunctuator, scanner, source)
                {
                    tok
                } else if let Ok(tok) = Token::parse_or_rewind(OtherPunctuator, scanner, source) {
                    tok
                } else {
                    return Err(UnexpectedChar);
                };
                Ok(Token {
                    ttype: Punctuator,
                    loc,
                    children: vec![tok],
                })
            }
            // OptionalChainingPunctuator ::
            // 	?. [lookahead ∉ DecimalDigit]
            OptionalChainingPunctuator => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take_str(".?") {
                    return Err(UnexpectedChar);
                }
                if let Ok(_) = Token::parse_or_rewind(DecimalDigit, scanner, source) {
                    return Err(UnexpectedChar);
                }
                Ok(Token {
                    ttype: OptionalChainingPunctuator,
                    loc,
                    children: vec![],
                })
            }
            // OtherPunctuator :: one of
            // 	{ ( ) [ ] . ... ; , < > <= >= == != === !== + - * % ** ++ -- << >> >>> & |
            // 	^ ! ~ && || ?? ? : = += -= *= %= **= <<= >>= >>>= &= |= ^= &&= ||= ??= =>
            OtherPunctuator => {
                if scanner.is_done() {
                    Err(UnexpectedEOF)
                } else if scanner.take_any(&[
                    "{", "(", ")", "[", "]", ".", "...", ";", ",", "<", ">", "<=", ">=", "==",
                    "!=", "===", "!==", "+", "-", "*", "%", "**", "++", "--", "<<", ">>", ">>>",
                    "&", "|", "^", "!", "~", "&&", "||", "??", "?", ":", "=", "+=", "-=", "*=",
                    "%=", "**=", "<<=", ">>=", ">>>=", "&=", "|=", "^=", "&&=", "||=", "??=", "=>",
                ]) {
                    Ok(Token {
                        ttype: OtherPunctuator,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // DivPunctuator ::
            // 	/
            // 	/=
            DivPunctuator => {
                if scanner.is_done() {
                    Err(UnexpectedEOF)
                } else if scanner.take(&'/') || scanner.take_str("/=") {
                    Ok(Token {
                        ttype: DivPunctuator,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // RightBracePunctuator ::
            // 	}
            RightBracePunctuator => {
                if scanner.is_done() {
                    Err(UnexpectedEOF)
                } else if scanner.take(&'}') {
                    Ok(Token {
                        ttype: RightBracePunctuator,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // DecimalDigit :: one of
            // 	0 1 2 3 4 5 6 7 8 9
            DecimalDigit => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take_any(&["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]) {
                    Ok(Token {
                        ttype: DecimalDigit,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // NullLiteral ::
            // 	null
            NullLiteral => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take_str("null") {
                    Ok(Token {
                        ttype: NullLiteral,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // BooleanLiteral ::
            // 	true
            // 	false
            BooleanLiteral => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take_str("true") || scanner.take_str("false") {
                    Ok(Token {
                        ttype: BooleanLiteral,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // NumericLiteral ::
            // 	DecimalLiteral
            // 	DecimalBigIntegerLiteral
            // 	NonDecimalIntegerLiteral_{[+Sep]}
            // 	NonDecimalIntegerLiteral_{[+Sep]} BigIntLiteralSuffix
            // 	LegacyOctalIntegerLiteral
            // The SourceCharacter immediately following a NumericLiteral must not be an IdentifierStart or DecimalDigit.
            NumericLiteral => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                // if we checked for `LegacyOctalIntegerLiteral` after `DecimalLiteral` it doesn't work
                // since technically the first 0 is a valid `DecimalLiteral`. this causes an error later
                // since `NumericLiteral` disallows digits after itself
                let children = if let Ok(tok) =
                    Token::parse_or_rewind(LegacyOctalIntegerLiteral, scanner, source)
                {
                    vec![tok]
                } else if let Ok(tok) =
                    Token::parse_or_rewind(DecimalBigIntegerLiteral, scanner, source)
                {
                    vec![tok]
                } else if let Ok(tok) =
                    Token::parse_or_rewind(NonDecimalIntegerLiteral_Sep, scanner, source)
                {
                    if let Ok(suffix) = Token::parse_or_rewind(BigIntLiteralSuffix, scanner, source)
                    {
                        vec![tok, suffix]
                    } else {
                        vec![tok]
                    }
                } else if let Ok(tok) = Token::parse_or_rewind(DecimalLiteral, scanner, source) {
                    vec![tok]
                } else {
                    return Err(UnexpectedChar);
                };
                if let Some(c) = scanner.peek() {
                    if c.is_ascii_digit()
                        || c == &'$'
                        || c == &'_'
                        || unicode_id_start::is_id_start(*c)
                    {
                        return Err(UnexpectedChar);
                    }
                }
                Ok(Token {
                    ttype: NumericLiteral,
                    loc,
                    children,
                })
            }
            // DecimalBigIntegerLiteral ::
            // 	0 BigIntLiteralSuffix
            // 	NonZeroDigit DecimalDigits_{[+Sep]opt} BigIntLiteralSuffix
            // 	NonZeroDigit NumericLiteralSeparator DecimalDigits_{[+Sep]} BigIntLiteralSuffix
            DecimalBigIntegerLiteral => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                let mut children = vec![];

                if !scanner.take(&'0') {
                    children.push(Token::parse_or_rewind(NonZeroDigit, scanner, source)?);
                    if let Ok(tok) =
                        Token::parse_or_rewind(NumericLiteralSeparator, scanner, source)
                    {
                        children.push(tok);
                        children.push(Token::parse_or_rewind(DecimalDigits_Sep, scanner, source)?);
                    } else if let Ok(tok) =
                        Token::parse_or_rewind(DecimalDigits_Sep, scanner, source)
                    {
                        children.push(tok);
                    }
                }

                children.push(Token::parse_or_rewind(
                    BigIntLiteralSuffix,
                    scanner,
                    source,
                )?);
                Ok(Token {
                    ttype: DecimalBigIntegerLiteral,
                    loc,
                    children,
                })
            }
            // NonDecimalIntegerLiteral_{[Sep]} ::
            // 	BinaryIntegerLiteral_{[?Sep]}
            // 	OctalIntegerLiteral_{[?Sep]}
            // 	HexIntegerLiteral_{[?Sep]}
            NonDecimalIntegerLiteral => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                let tok = if let Ok(tok) =
                    Token::parse_or_rewind(BinaryIntegerLiteral, scanner, source)
                {
                    tok
                } else if let Ok(tok) = Token::parse_or_rewind(OctalIntegerLiteral, scanner, source)
                {
                    tok
                } else if let Ok(tok) = Token::parse_or_rewind(HexIntegerLiteral, scanner, source) {
                    tok
                } else {
                    return Err(UnexpectedChar);
                };
                Ok(Token {
                    ttype: NonDecimalIntegerLiteral,
                    loc,
                    children: vec![tok],
                })
            }
            NonDecimalIntegerLiteral_Sep => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                let tok = if let Ok(tok) =
                    Token::parse_or_rewind(BinaryIntegerLiteral_Sep, scanner, source)
                {
                    tok
                } else if let Ok(tok) =
                    Token::parse_or_rewind(OctalIntegerLiteral_Sep, scanner, source)
                {
                    tok
                } else if let Ok(tok) =
                    Token::parse_or_rewind(HexIntegerLiteral_Sep, scanner, source)
                {
                    tok
                } else {
                    return Err(UnexpectedChar);
                };
                Ok(Token {
                    ttype: NonDecimalIntegerLiteral_Sep,
                    loc,
                    children: vec![tok],
                })
            }
            // BigIntLiteralSuffix ::
            // 	n
            BigIntLiteralSuffix => {
                if scanner.is_done() {
                    Err(UnexpectedEOF)
                } else if scanner.take(&'n') {
                    Ok(Token {
                        ttype: BigIntLiteralSuffix,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // DecimalLiteral ::
            // . DecimalDigits_{[+Sep]} ExponentPart_{[+Sep]opt}
            // DecimalIntegerLiteral . DecimalDigits_{[+Sep]opt} ExponentPart_{[+Sep]opt}
            // DecimalIntegerLiteral ExponentPart_{[+Sep]opt}
            DecimalLiteral => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take(&'.') {
                    let mut children =
                        vec![Token::parse_or_rewind(DecimalDigits_Sep, scanner, source)?];
                    if let Ok(tok) = Token::parse_or_rewind(ExponentPart_Sep, scanner, source) {
                        children.push(tok);
                    }
                    return Ok(Token {
                        ttype: DecimalLiteral,
                        loc,
                        children,
                    });
                }
                let mut children = vec![Token::parse_or_rewind(
                    DecimalIntegerLiteral,
                    scanner,
                    source,
                )?];
                if scanner.take(&'.') {
                    if let Ok(tok) = Token::parse_or_rewind(DecimalDigits_Sep, scanner, source) {
                        children.push(tok);
                    }
                    if let Ok(tok) = Token::parse_or_rewind(ExponentPart_Sep, scanner, source) {
                        children.push(tok);
                    }
                    return Ok(Token {
                        ttype: DecimalLiteral,
                        loc,
                        children,
                    });
                } else if let Ok(tok) = Token::parse_or_rewind(ExponentPart_Sep, scanner, source) {
                    children.push(tok);
                }

                Ok(Token {
                    ttype: DecimalLiteral,
                    loc,
                    children,
                })
            }
            // DecimalIntegerLiteral ::
            // 0
            // NonZeroDigit
            // NonZeroDigit NumericLiteralSeparator_{opt} DecimalDigits_{[+Sep]}
            // NonOctalDecimalIntegerLiteral
            DecimalIntegerLiteral => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take(&'0') {
                    return Ok(Token {
                        ttype: DecimalIntegerLiteral,
                        loc,
                        children: vec![],
                    });
                }
                if let Ok(tok) =
                    Token::parse_or_rewind(NonOctalDecimalIntegerLiteral, scanner, source)
                {
                    return Ok(Token {
                        ttype: DecimalIntegerLiteral,
                        loc,
                        children: vec![tok],
                    });
                }
                let Ok(tok) = Token::parse_or_rewind(NonZeroDigit, scanner, source) else {
                    return Err(UnexpectedChar);
				};
                if let Ok(sep) = Token::parse_or_rewind(NumericLiteralSeparator, scanner, source) {
                    return Ok(Token {
                        ttype: DecimalIntegerLiteral,
                        loc,
                        children: vec![
                            tok,
                            sep,
                            Token::parse_or_rewind(DecimalDigits_Sep, scanner, source)?,
                        ],
                    });
                }
                if let Ok(digs) = Token::parse_or_rewind(DecimalDigits_Sep, scanner, source) {
                    return Ok(Token {
                        ttype: DecimalIntegerLiteral,
                        loc,
                        children: vec![tok, digs],
                    });
                }
                Ok(Token {
                    ttype: DecimalIntegerLiteral,
                    loc,
                    children: vec![tok],
                })
            }
            // DecimalDigits_{[Sep]} ::
            // DecimalDigit
            // DecimalDigits_{[?Sep]} DecimalDigit
            // [+Sep] DecimalDigits_{[+Sep]} NumericLiteralSeparator DecimalDigit
            DecimalDigits => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                let mut children = vec![Token::parse_or_rewind(DecimalDigit, scanner, source)?];
                while let Ok(dig) = Token::parse_or_rewind(DecimalDigit, scanner, source) {
                    children.push(dig);
                }
                Ok(Token {
                    ttype: DecimalDigits,
                    loc,
                    children,
                })
            }
            DecimalDigits_Sep => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                let mut children = vec![Token::parse_or_rewind(DecimalDigit, scanner, source)?];
                let mut sep_res = Token::parse_or_rewind(NumericLiteralSeparator, scanner, source);
                while let Ok(dig) = Token::parse_or_rewind(DecimalDigit, scanner, source) {
                    children.push(dig);
                    sep_res = Token::parse_or_rewind(NumericLiteralSeparator, scanner, source);
                }
                if sep_res.is_ok() {
                    return Err(UnexpectedChar);
                }
                Ok(Token {
                    ttype: DecimalDigits_Sep,
                    loc,
                    children,
                })
            }
            // NonZeroDigit :: one of
            // 	1 2 3 4 5 6 7 8 9
            NonZeroDigit => {
                if scanner.is_done() {
                    Err(UnexpectedEOF)
                } else if scanner.take_any(&["1", "2", "3", "4", "5", "6", "7", "8", "9"]) {
                    Ok(Token {
                        ttype: NonZeroDigit,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // ExponentPart_{[Sep]} ::
            // 	ExponentIndicator SignedInteger_{[?Sep]}
            ExponentPart => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                Ok(Token {
                    ttype: ExponentPart,
                    loc,
                    children: vec![
                        Token::parse_or_rewind(ExponentIndicator, scanner, source)?,
                        Token::parse_or_rewind(SignedInteger, scanner, source)?,
                    ],
                })
            }
            ExponentPart_Sep => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                Ok(Token {
                    ttype: ExponentPart_Sep,
                    loc,
                    children: vec![
                        Token::parse_or_rewind(ExponentIndicator, scanner, source)?,
                        Token::parse_or_rewind(SignedInteger_Sep, scanner, source)?,
                    ],
                })
            }
            // ExponentIndicator :: one of
            // 	e E
            ExponentIndicator => {
                if scanner.is_done() {
                    Err(UnexpectedEOF)
                } else if scanner.take(&'e') || scanner.take(&'E') {
                    Ok(Token {
                        ttype: ExponentIndicator,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // SignedInteger_{[Sep]} ::
            // 	DecimalDigits_{[?Sep]}
            // 	+ DecimalDigits_{[?Sep]}
            // 	- DecimalDigits_{[?Sep]}
            SignedInteger | SignedInteger_Sep => {
                let dig_type = if ttype == SignedInteger {
                    DecimalDigits
                } else if ttype == SignedInteger_Sep {
                    DecimalDigits_Sep
                } else {
                    unreachable!()
                };
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take(&'+') {
                    let _ = scanner.take(&'-');
                }
                Ok(Token {
                    ttype,
                    loc,
                    children: vec![Token::parse_or_rewind(dig_type, scanner, source)?],
                })
            }
            // BinaryIntegerLiteral[Sep] ::
            // 	0b BinaryDigits_{[?Sep]}
            // 	0B BinaryDigits_{[?Sep]}
            BinaryIntegerLiteral | BinaryIntegerLiteral_Sep => {
                let dig_type = if ttype == BinaryIntegerLiteral {
                    BinaryDigits
                } else if ttype == BinaryIntegerLiteral_Sep {
                    BinaryDigits_Sep
                } else {
                    unreachable!()
                };
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take_str("0b") || scanner.take_str("0B") {
                    Ok(Token {
                        ttype,
                        loc,
                        children: vec![Token::parse_or_rewind(dig_type, scanner, source)?],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // BinaryDigits_{[Sep]} ::
            // 	BinaryDigit
            // 	BinaryDigits_{[?Sep]} BinaryDigit
            // 	[+Sep] BinaryDigits[+Sep] NumericLiteralSeparator BinaryDigit
            BinaryDigits => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                let mut children = vec![Token::parse_or_rewind(BinaryDigit, scanner, source)?];
                while let Ok(dig) = Token::parse_or_rewind(BinaryDigit, scanner, source) {
                    children.push(dig);
                }
                Ok(Token {
                    ttype: BinaryDigits,
                    loc,
                    children,
                })
            }
            BinaryDigits_Sep => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                let mut children = vec![Token::parse_or_rewind(BinaryDigit, scanner, source)?];
                let mut sep_res = Token::parse_or_rewind(NumericLiteralSeparator, scanner, source);
                while let Ok(dig) = Token::parse_or_rewind(BinaryDigit, scanner, source) {
                    children.push(dig);
                    sep_res = Token::parse_or_rewind(NumericLiteralSeparator, scanner, source);
                }
                if sep_res.is_ok() {
                    return Err(UnexpectedChar);
                }
                Ok(Token {
                    ttype: BinaryDigits_Sep,
                    loc,
                    children,
                })
            }
            // BinaryDigit :: one of
            // 	0 1
            BinaryDigit => {
                if scanner.is_done() {
                    Err(UnexpectedEOF)
                } else if scanner.take(&'0') || scanner.take(&'1') {
                    Ok(Token {
                        ttype: BinaryDigit,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // OctalIntegerLiteral_{[Sep]} ::
            // 	0o OctalDigits_{[?Sep]}
            // 	0O OctalDigits_{[?Sep]}
            OctalIntegerLiteral | OctalIntegerLiteral_Sep => {
                let dig_type = if ttype == OctalIntegerLiteral {
                    OctalDigits
                } else if ttype == OctalIntegerLiteral_Sep {
                    OctalDigits_Sep
                } else {
                    unreachable!()
                };
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take_str("0o") || scanner.take_str("0O") {
                    Ok(Token {
                        ttype,
                        loc,
                        children: vec![Token::parse_or_rewind(dig_type, scanner, source)?],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // OctalDigits_{[Sep]} ::
            // 	OctalDigit
            // 	OctalDigits_{[?Sep]} OctalDigit
            // 	[+Sep] OctalDigits[+Sep] NumericLiteralSeparator OctalDigit
            OctalDigits => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                let mut children = vec![Token::parse_or_rewind(OctalDigit, scanner, source)?];
                while let Ok(dig) = Token::parse_or_rewind(OctalDigit, scanner, source) {
                    children.push(dig);
                }
                Ok(Token {
                    ttype: OctalDigits,
                    loc,
                    children,
                })
            }
            OctalDigits_Sep => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                let mut children = vec![Token::parse_or_rewind(OctalDigit, scanner, source)?];
                let mut sep_res = Token::parse_or_rewind(NumericLiteralSeparator, scanner, source);
                while let Ok(dig) = Token::parse_or_rewind(OctalDigit, scanner, source) {
                    children.push(dig);
                    sep_res = Token::parse_or_rewind(NumericLiteralSeparator, scanner, source);
                }
                if sep_res.is_ok() {
                    return Err(UnexpectedChar);
                }
                Ok(Token {
                    ttype: OctalDigits_Sep,
                    loc,
                    children,
                })
            }
            // LegacyOctalIntegerLiteral ::
            // 	0 OctalDigit
            // 	LegacyOctalIntegerLiteral OctalDigit
            LegacyOctalIntegerLiteral => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take(&'0') {
                    return Err(UnexpectedChar);
                }
                let mut children = vec![Token::parse_or_rewind(OctalDigit, scanner, source)?];
                while let Ok(dig) = Token::parse_or_rewind(OctalDigit, scanner, source) {
                    children.push(dig);
                }
                Ok(Token {
                    ttype: LegacyOctalIntegerLiteral,
                    loc,
                    children,
                })
            }
            // NonOctalDecimalIntegerLiteral ::
            // 	0 NonOctalDigit
            // 	LegacyOctalLikeDecimalIntegerLiteral NonOctalDigit
            // 	NonOctalDecimalIntegerLiteral DecimalDigit
            // ============= summary =============
            // a 0 followed by any amount of `OctalDigit`s
            // followed by a `NonOctalDigit`
            // followed by any amount of `DecimalDigit`s
            NonOctalDecimalIntegerLiteral => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take(&'0') {
                    return Err(UnexpectedChar);
                }
                let mut children = vec![];
                while let Ok(tok) = Token::parse_or_rewind(OctalDigit, scanner, source) {
                    children.push(tok);
                }
                children.push(Token::parse_or_rewind(NonOctalDigit, scanner, source)?);
                while let Ok(tok) = Token::parse_or_rewind(DecimalDigit, scanner, source) {
                    children.push(tok);
                }
                Ok(Token {
                    ttype: NonOctalDecimalIntegerLiteral,
                    loc,
                    children,
                })
            }
            // LegacyOctalLikeDecimalIntegerLiteral ::
            // 	0 OctalDigit
            // 	LegacyOctalLikeDecimalIntegerLiteral OctalDigit
            LegacyOctalLikeDecimalIntegerLiteral => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take(&'0') {
                    return Err(UnexpectedChar);
                }
                let mut children = vec![Token::parse_or_rewind(OctalDigit, scanner, source)?];
                while let Ok(dig) = Token::parse_or_rewind(OctalDigit, scanner, source) {
                    children.push(dig);
                }
                Ok(Token {
                    ttype: LegacyOctalLikeDecimalIntegerLiteral,
                    loc,
                    children,
                })
            }
            // OctalDigit :: one of
            // 	0 1 2 3 4 5 6 7
            OctalDigit => {
                if scanner.is_done() {
                    Err(UnexpectedEOF)
                } else if scanner.take_any(&["0", "1", "2", "3", "4", "5", "6", "7"]) {
                    Ok(Token {
                        ttype: OctalDigit,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // NonOctalDigit :: one of
            // 	8 9
            NonOctalDigit => {
                if scanner.is_done() {
                    Err(UnexpectedEOF)
                } else if scanner.take(&'8') || scanner.take(&'9') {
                    Ok(Token {
                        ttype: BinaryDigit,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // HexIntegerLiteral_{[Sep]} ::
            // 	0x HexDigits_{[?Sep]}
            // 	0X HexDigits_{[?Sep]}
            HexIntegerLiteral | HexIntegerLiteral_Sep => {
                let dig_type = if ttype == HexIntegerLiteral {
                    HexDigits
                } else if ttype == HexIntegerLiteral_Sep {
                    HexDigits_Sep
                } else {
                    unreachable!()
                };
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take_str("0x") || scanner.take_str("0X") {
                    Ok(Token {
                        ttype,
                        loc,
                        children: vec![Token::parse_or_rewind(dig_type, scanner, source)?],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // SingleEscapeCharacter :: one of
            //     ' " \ b f n r t v
            SingleEscapeCharacter => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take_any(&["\'", "\"", "\\", "b", "f", "n", "r", "t", "v"]) {
                    Ok(Token {
                        ttype,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // NonZeroOctalDigit ::
            //     OctalDigit but not 0
            NonZeroOctalDigit => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take_any(&["1", "2", "3", "4", "5", "6", "7"]) {
                    Ok(Token {
                        ttype,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // ZeroToThree :: one of
            //     0 1 2 3
            ZeroToThree => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take_any(&["0", "1", "2", "3"]) {
                    Ok(Token {
                        ttype,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // FourToSeven :: one of
            //     4 5 6 7
            FourToSeven => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take_any(&["4", "5", "6", "7"]) {
                    Ok(Token {
                        ttype,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // NonOctalDecimalEscapeSequence :: one of
            //     8 9
            NonOctalDecimalEscapeSequence => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take_any(&["8", "9"]) {
                    Ok(Token {
                        ttype,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // LegacyOctalEscapeSequence ::
            //     0 [lookahead ∈ { 8, 9 }]
            //     NonZeroOctalDigit [lookahead ∉ OctalDigit]
            //     ZeroToThree OctalDigit [lookahead ∉ OctalDigit]
            //     FourToSeven OctalDigit
            //     ZeroToThree OctalDigit OctalDigit
            LegacyOctalEscapeSequence => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take(&'0') && matches!(scanner.peek(), Some(&'8') | Some(&'9')) {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![],
                    });
                }
                if let Ok(nzod) = Token::parse_or_rewind(NonZeroOctalDigit, scanner, source) {
                    if Token::parse_or_rewind(OctalDigit, scanner, source).is_err() {
                        return Ok(Token {
                            ttype,
                            loc,
                            children: vec![nzod],
                        });
                    }
                    return Err(UnexpectedChar);
                }
                if let Ok(ztt) = Token::parse_or_rewind(ZeroToThree, scanner, source) {
                    let od1 = Token::parse_or_rewind(OctalDigit, scanner, source)?;
                    if let Ok(od2) = Token::parse_or_rewind(OctalDigit, scanner, source) {
                        return Ok(Token {
                            ttype,
                            loc,
                            children: vec![ztt, od1, od2],
                        });
                    }
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![ztt, od1],
                    });
                }
                Ok(Token {
                    ttype,
                    loc,
                    children: vec![
                        Token::parse_or_rewind(FourToSeven, scanner, source)?,
                        Token::parse_or_rewind(OctalDigit, scanner, source)?,
                    ],
                })
            }
            // LineContinuation ::
            //     \ LineTerminatorSequence
            LineContinuation => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take(&'\\') {
                    Ok(Token {
                        ttype,
                        loc,
                        children: vec![Token::parse_or_rewind(
                            LineTerminatorSequence,
                            scanner,
                            source,
                        )?],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // EscapeCharacter ::
            //     SingleEscapeCharacter
            //     DecimalDigit
            //     x
            //     u
            EscapeCharacter => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take(&'x') || scanner.take(&'u') {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![],
                    });
                }
                if let Ok(tok) = Token::parse_or_rewind(SingleEscapeCharacter, scanner, source) {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![tok],
                    });
                }
                if let Ok(tok) = Token::parse_or_rewind(DecimalDigit, scanner, source) {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![tok],
                    });
                }
                Err(UnexpectedChar)
            }
            // NonEscapeCharacter ::
            //     SourceCharacter but not one of EscapeCharacter or LineTerminator
            NonEscapeCharacter => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if Token::parse_or_rewind(EscapeCharacter, scanner, source).is_err()
                    && Token::parse_or_rewind(LineTerminator, scanner, source).is_err()
                {
                    Ok(Token {
                        ttype,
                        loc,
                        children: vec![],
                    })
                } else {
                    Err(UnexpectedChar)
                }
            }
            // CharacterEscapeSequence ::
            //     SingleEscapeCharacter
            //     NonEscapeCharacter
            CharacterEscapeSequence => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if let Ok(tok) = Token::parse_or_rewind(SingleEscapeCharacter, scanner, source) {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![tok],
                    });
                }
                if let Ok(tok) = Token::parse_or_rewind(NonEscapeCharacter, scanner, source) {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![tok],
                    });
                }
                Err(UnexpectedChar)
            }
            // HexEscapeSequence ::
            //     x HexDigit HexDigit
            HexEscapeSequence => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if !scanner.take(&'x') {
                    return Err(UnexpectedChar);
                }
                Ok(Token {
                    ttype,
                    loc,
                    children: vec![
                        Token::parse_or_rewind(HexDigit, scanner, source)?,
                        Token::parse_or_rewind(HexDigit, scanner, source)?,
                    ],
                })
            }
            // EscapeSequence ::
            //     CharacterEscapeSequence
            //     0 [lookahead ∉ DecimalDigit]
            //     LegacyOctalEscapeSequence
            //     NonOctalDecimalEscapeSequence
            //     HexEscapeSequence
            //     UnicodeEscapeSequence
            EscapeSequence => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.peek() == Some(&'0')
                    && !matches!(
                        scanner.peek_n(1),
                        Some(&'0')
                            | Some(&'1')
                            | Some(&'2')
                            | Some(&'3')
                            | Some(&'4')
                            | Some(&'5')
                            | Some(&'6')
                            | Some(&'7')
                            | Some(&'8')
                            | Some(&'9')
                    )
                {
                    scanner.pop();
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![],
                    });
                }
                if let Ok(tok) = Token::parse_or_rewind(CharacterEscapeSequence, scanner, source) {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![tok],
                    });
                }
                if let Ok(tok) = Token::parse_or_rewind(LegacyOctalEscapeSequence, scanner, source)
                {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![tok],
                    });
                }
                if let Ok(tok) =
                    Token::parse_or_rewind(NonOctalDecimalEscapeSequence, scanner, source)
                {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![tok],
                    });
                }
                if let Ok(tok) = Token::parse_or_rewind(HexEscapeSequence, scanner, source) {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![tok],
                    });
                }
                if let Ok(tok) = Token::parse_or_rewind(UnicodeEscapeSequence, scanner, source) {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![tok],
                    });
                }
                Err(UnexpectedChar)
            }
            // DoubleStringCharacter ::
            //     SourceCharacter but not one of " or \ or LineTerminator
            //     <LS>
            //     <PS>
            //     \ EscapeSequence
            //     LineContinuation
            // SingleStringCharacter ::
            //     SourceCharacter but not one of ' or \ or LineTerminator
            //     <LS>
            //     <PS>
            //     \ EscapeSequence
            //     LineContinuation
            DoubleStringCharacter | SingleStringCharacter => {
                let str_ch = if ttype == DoubleStringCharacter {
                    '"'
                } else {
                    '\''
                };
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take(&LS) || scanner.take(&PS) {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![],
                    });
                }
                if let Ok(tok) = Token::parse_or_rewind(LineContinuation, scanner, source) {
                    return Ok(Token {
                        ttype,
                        loc,
                        children: vec![tok],
                    });
                }
                if scanner.take(&'\\') {
                    if let Ok(tok) = Token::parse_or_rewind(EscapeSequence, scanner, source) {
                        return Ok(Token {
                            ttype,
                            loc,
                            children: vec![tok],
                        });
                    }
                    return Err(UnexpectedChar);
                }
                if scanner.take(&str_ch) {
                    return Err(UnexpectedChar);
                }
                if let Ok(_) = Token::parse_or_rewind(LineTerminator, scanner, source) {
                    return Err(UnexpectedChar);
                }
                scanner.pop();
                Ok(Token {
                    ttype,
                    loc,
                    children: vec![],
                })
            }
            // DoubleStringCharacters ::
            //     DoubleStringCharacter DoubleStringCharacters_{opt}
            // SingleStringCharacters ::
            //     SingleStringCharacter SingleStringCharacters_{opt}
            DoubleStringCharacters | SingleStringCharacters => {
                let tt = if ttype == DoubleStringCharacters {
                    DoubleStringCharacter
                } else {
                    SingleStringCharacter
                };
                let mut children = vec![Token::parse_or_rewind(tt, scanner, source)?];
                while let Ok(tok) = Token::parse_or_rewind(tt, scanner, source) {
                    children.push(tok);
                }
                Ok(Token {
                    ttype,
                    loc,
                    children,
                })
            }
            // StringLiteral ::
            //     " DoubleStringCharacters_{opt} "
            //     ' SingleStringCharacters_{opt} '
            StringLiteral => {
                if scanner.is_done() {
                    return Err(UnexpectedEOF);
                }
                if scanner.take(&'"') {
                    let children = if let Ok(chars) =
                        Token::parse_or_rewind(DoubleStringCharacters, scanner, source)
                    {
                        vec![chars]
                    } else {
                        vec![]
                    };
                    if !scanner.take(&'"') {
                        return Err(UnexpectedChar);
                    }
                    return Ok(Token {
                        ttype,
                        loc,
                        children,
                    });
                }
                if scanner.take(&'\'') {
                    let children = if let Ok(chars) =
                        Token::parse_or_rewind(SingleStringCharacters, scanner, source)
                    {
                        vec![chars]
                    } else {
                        vec![]
                    };
                    if !scanner.take(&'\'') {
                        return Err(UnexpectedChar);
                    }
                    return Ok(Token {
                        ttype,
                        loc,
                        children,
                    });
                }
                Err(UnexpectedChar)
            }
            Template => todo!(),
        }
    }

    /// [<u>MV</u> (Mathematical Value)](https://262.ecma-international.org/13.0/#sec-string-literals-static-semantics-mv)
    pub(crate) fn mv(&self, source: &str) -> f64 {
        let count_digits = |tok: &Token| -> f64 {
            let mut d = 0.;
            for ch in &tok.children {
                if ch.ttype != NumericLiteralSeparator {
                    d += 1.;
                }
            }
            d
        };
        match self.ttype {
            // - Hex4Digits :: HexDigit HexDigit HexDigit HexDigit
            // (the MV of the first HexDigit × 16^3) plus
            // (the MV of the second HexDigit × 16^2) plus
            // (the MV of the third HexDigit × 16^1) plus
            // the MV of the fourth HexDigit
            Hex4Digits => {
                4096. * self.children[0].mv(source)
                    + 256. * self.children[1].mv(source)
                    + 16. * self.children[2].mv(source)
                    + self.children[3].mv(source)
            }
            // - HexDigits :: HexDigits HexDigit
            // (the MV of HexDigits × 16) plus the MV of HexDigit
            // - HexDigits :: HexDigits NumericLiteralSeparator HexDigit
            // (the MV of HexDigits × 16) plus the MV of HexDigit.
            HexDigits | HexDigits_Sep => {
                let mut val = 0.;
                for tok in &self.children {
                    match tok.ttype {
                        HexDigit => {
                            val *= 16.;
                            val += tok.mv(source);
                        }
                        NumericLiteralSeparator => {}
                        tt => unreachable!("`HexDigits_{{[Sep]}}` should contain only `HexDigit` or `NumericLiteralSeparator`, found {tt:?}"),
                    }
                }
                val
            }
            CodePoint => self.children[0].mv(source),
            // - DecimalLiteral :: DecimalIntegerLiteral . DecimalDigits
            // MV of DecimalIntegerLiteral plus (the MV of DecimalDigits × 10-n), where n is the number of code points in DecimalDigits, excluding all occurrences of NumericLiteralSeparator
            // - DecimalLiteral :: DecimalIntegerLiteral . ExponentPart
            // MV of DecimalIntegerLiteral × 10e, where e is the MV of ExponentPart
            // - DecimalLiteral :: DecimalIntegerLiteral . DecimalDigits ExponentPart
            // (the MV of DecimalIntegerLiteral plus (the MV of DecimalDigits × 10-n)) × 10e, where n is the number of code points in DecimalDigits, excluding all occurrences of NumericLiteralSeparator and e is the MV of ExponentPart
            // - DecimalLiteral :: . DecimalDigits
            // the MV of DecimalDigits × 10^(-n), where n is the number of code points in DecimalDigits, excluding all occurrences of NumericLiteralSeparator
            // - DecimalLiteral :: . DecimalDigits ExponentPart
            // the MV of DecimalDigits × 10^(e - n), where n is the number of code points in DecimalDigits, excluding all occurrences of NumericLiteralSeparator, and e is the MV of ExponentPart
            // - DecimalLiteral :: DecimalIntegerLiteral ExponentPart
            // MV of DecimalIntegerLiteral × 10^e, where e is the MV of ExponentPart
            DecimalLiteral => {
                if source.chars().nth(self.loc).unwrap() == '.' {
                    assert_eq!(self.children[0].ttype, DecimalDigits_Sep);
                    let dec_dig_mv = self.children[0].mv(source);
                    let n = count_digits(&self.children[0]);
                    let exp = if self.children.len() == 2 {
                        assert_eq!(self.children[1].ttype, ExponentPart_Sep);
                        self.children[1].mv(source) - n
                    } else {
                        -n
                    };
                    dec_dig_mv * 10_f64.powf(exp)
                } else {
                    assert_eq!(self.children[0].ttype, DecimalIntegerLiteral);
                    let dec_lit_mv = self.children[0].mv(source);
                    if self.children.len() == 1 {
                        dec_lit_mv
                    } else {
                        if self.children[1].ttype == ExponentPart_Sep {
                            dec_lit_mv * 10_f64.powf(self.children[1].mv(source))
                        } else if self.children[1].ttype == DecimalDigits_Sep {
                            let dec_dig_mv = self.children[1].mv(source);
                            let n = count_digits(&self.children[1]);
                            if self.children.len() == 3 {
                                assert_eq!(self.children[2].ttype, ExponentPart_Sep);
                                let e = self.children[2].mv(source);
                                (dec_lit_mv + (dec_dig_mv * 10_f64.powf(-n))) * 10_f64.powf(e)
                            } else {
                                dec_lit_mv + (dec_dig_mv * 10_f64.powf(-n))
                            }
                        } else {
                            unreachable!()
                        }
                    }
                }
            }
            // - DecimalIntegerLiteral :: 0
            // 0
            // - DecimalIntegerLiteral :: NonZeroDigit NumericLiteralSeparatoropt DecimalDigits
            // (the MV of NonZeroDigit × 10^n) plus the MV of DecimalDigits, where n is the number of code points in DecimalDigits, excluding all occurrences of NumericLiteralSeparator
            DecimalIntegerLiteral => {
                if self.children.len() == 0 {
                    0.0
                } else if self.children.len() == 1 {
                    self.children[0].mv(source)
                } else if self.children.len() == 2 {
                    assert_eq!(self.children[0].ttype, NonZeroDigit);
                    assert_eq!(self.children[1].ttype, DecimalDigits_Sep);
                    self.children[0].mv(source) * 10_f64.powf(count_digits(&self.children[1]))
                        + self.children[1].mv(source)
                } else if self.children.len() == 3 {
                    assert_eq!(self.children[0].ttype, NonZeroDigit);
                    assert_eq!(self.children[1].ttype, NumericLiteralSeparator);
                    assert_eq!(self.children[2].ttype, DecimalDigits_Sep);
                    self.children[0].mv(source) * 10_f64.powf(count_digits(&self.children[2]))
                        + self.children[2].mv(source)
                } else {
                    unreachable!()
                }
            }
            // - DecimalDigits :: DecimalDigits DecimalDigit
            // (the MV of DecimalDigits × 10) plus the MV of DecimalDigit
            // - DecimalDigits :: DecimalDigits NumericLiteralSeparator DecimalDigit
            // (the MV of DecimalDigits × 10) plus the MV of DecimalDigit
            DecimalDigits | DecimalDigits_Sep => {
                let mut val = 0.0;
                for ch in &self.children {
                    if ch.ttype == NumericLiteralSeparator {
                        continue;
                    }
                    assert_eq!(ch.ttype, DecimalDigit);
                    val *= 10.0;
                    val += ch.mv(source);
                }
                val
            }
            // - ExponentPart :: ExponentIndicator SignedInteger
            // the MV of SignedInteger
            ExponentPart | ExponentPart_Sep => {
                assert_eq!(self.children[0].ttype, ExponentIndicator);
                assert!(
                    self.children[1].ttype == SignedInteger
                        || self.children[1].ttype == SignedInteger_Sep
                );
                self.children[1].mv(source)
            }
            // - SignedInteger :: - DecimalDigits
            // the negative of the MV of DecimalDigits
            SignedInteger | SignedInteger_Sep => {
                if source.chars().nth(self.loc).unwrap() == '-' {
                    -self.children[0].mv(source)
                } else {
                    self.children[0].mv(source)
                }
            }
            DecimalDigit | OctalDigit | BinaryDigit | HexDigit | NonZeroDigit | NonOctalDigit => {
                match source.chars().nth(self.loc).unwrap() {
                    '0' => 0.,
                    '1' => 1.,
                    '2' => 2.,
                    '3' => 3.,
                    '4' => 4.,
                    '5' => 5.,
                    '6' => 6.,
                    '7' => 7.,
                    '8' => 8.,
                    '9' => 9.,
                    'a' | 'A' => 10.,
                    'b' | 'B' => 11.,
                    'c' | 'C' => 12.,
                    'd' | 'D' => 13.,
                    'e' | 'E' => 14.,
                    'f' | 'F' => 15.,
                    c => unreachable!("expected digit, found {c}"),
                }
            }
            // - BinaryDigits :: BinaryDigits BinaryDigit
            // (the MV of BinaryDigits × 2) plus the MV of BinaryDigit
            // - BinaryDigits :: BinaryDigits NumericLiteralSeparator BinaryDigit
            // (the MV of BinaryDigits × 2) plus the MV of BinaryDigit
            BinaryDigits | BinaryDigits_Sep => {
                let mut val = 0.0;
                for ch in &self.children {
                    if ch.ttype == NumericLiteralSeparator {
                        continue;
                    }
                    assert_eq!(ch.ttype, BinaryDigit);
                    val *= 2.0;
                    val += ch.mv(source);
                }
                val
            }
            // - OctalDigits :: OctalDigits OctalDigit
            // (the MV of OctalDigits × 8) plus the MV of OctalDigit
            // - OctalDigits :: OctalDigits NumericLiteralSeparator OctalDigit
            // (the MV of OctalDigits × 8) plus the MV of OctalDigit
            // - LegacyOctalIntegerLiteral :: LegacyOctalIntegerLiteral OctalDigit
            // (the MV of LegacyOctalIntegerLiteral times 8) plus the MV of OctalDigit
            OctalDigits | OctalDigits_Sep | LegacyOctalIntegerLiteral => {
                let mut val = 0.0;
                for ch in &self.children {
                    if ch.ttype == NumericLiteralSeparator {
                        continue;
                    }
                    assert_eq!(ch.ttype, OctalDigit);
                    val *= 8.0;
                    val += ch.mv(source);
                }
                val
            }
            // - NonOctalDecimalIntegerLiteral :: LegacyOctalLikeDecimalIntegerLiteral NonOctalDigit
            // (the MV of LegacyOctalLikeDecimalIntegerLiteral times 10) plus the MV of NonOctalDigit
            // - NonOctalDecimalIntegerLiteral :: NonOctalDecimalIntegerLiteral DecimalDigit
            // (the MV of NonOctalDecimalIntegerLiteral times 10) plus the MV of DecimalDigit
            // - LegacyOctalLikeDecimalIntegerLiteral :: LegacyOctalLikeDecimalIntegerLiteral OctalDigit
            // (the MV of LegacyOctalLikeDecimalIntegerLiteral times 10) plus the MV of OctalDigit
            NonOctalDecimalIntegerLiteral | LegacyOctalLikeDecimalIntegerLiteral => {
                let mut val = 0.0;
                for ch in &self.children {
                    if ch.ttype == NumericLiteralSeparator {
                        continue;
                    }
                    val *= 10.0;
                    val += ch.mv(source);
                }
                val
            }
            t => unreachable!("mv is not implemented for {t:?}"),
        }
        /*
            The multiline comment is for easy collapsing of the whole section
            To be implemented:
            // - LegacyOctalEscapeSequence :: 0
            // 0
        */
    }

    /// [<u>IdentifierCodePoint</u>](https://262.ecma-international.org/13.0/#sec-identifiercodepoint)
    ///
    /// _IdentifierStart_ :: _IdentifierStartChar_
    ///
    /// 1. Return the code point matched by _IdentifierStartChar_.
    ///
    /// _IdentifierPart_ :: _IdentifierPartChar_
    ///
    /// 1. Return the code point matched by _IdentifierPartChar_.
    ///
    /// _UnicodeEscapeSequence_ :: **u** _Hex4Digits_
    ///
    /// 1. Return the code point whose numeric value is the <u>MV</u> of _Hex4Digits_.
    ///
    /// _UnicodeEscapeSequence_ :: **u{** _CodePoint_ **}**
    ///
    /// 1. Return the code point whose numeric value is the <u>MV</u> of _CodePoint_.
    fn identifier_code_point(&self, source: &str) -> char {
        match self.ttype {
			IdentifierStart if self.children[0].ttype == IdentifierStartChar => source.chars().nth(self.loc).unwrap(),
			IdentifierPart if self.children[0].ttype == IdentifierPartChar => source.chars().nth(self.loc).unwrap(),
			UnicodeEscapeSequence => char::from_u32(self.children[0].mv(source).round() as u32).unwrap(),
			_ => unreachable!("identifier_code_point is only implemented for IdentifierStart, IdentifierPart and UnicodeEscapeSequence")
		}
    }

    /// [<u>IdentifierCodePoints</u>](https://262.ecma-international.org/13.0/#sec-identifiercodepoints)
    ///
    /// _IdentifierName_ :: _IdentifierStart_
    ///
    /// 1. Let cp be <u>IdentifierCodePoint</u> of _IdentifierStart_.
    /// 2. Return « cp ».
    ///
    /// _IdentifierName_ :: _IdentifierName_ _IdentifierPart_
    /// 1. Let cps be <u>IdentifierCodePoints</u> of the derived _IdentifierName_.
    /// 2. Let cp be <u>IdentifierCodePoint</u> of _IdentifierPart_.
    /// 3. Return the list-concatenation of cps and « cp ».
    fn identifier_code_points(&self, source: &str) -> Vec<char> {
        assert_eq!(self.ttype, IdentifierName);
        self.children
            .iter()
            .map(|t| t.identifier_code_point(source))
            .collect()
    }
}
