#![allow(dead_code)]

pub(crate) const ZWNJ: char = '\u{200C}';
pub(crate) const ZWJ: char = '\u{200D}';
pub(crate) const ZWNBSP: char = '\u{FEFF}';

pub(crate) const TAB: char = '\u{0009}';
pub(crate) const VT: char = '\u{000B}';
pub(crate) const FF: char = '\u{000C}';
pub(crate) const LF: char = '\u{000A}';
pub(crate) const CR: char = '\u{000D}';
pub(crate) const LS: char = '\u{2028}';
pub(crate) const PS: char = '\u{2029}';
