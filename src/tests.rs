use super::*;
use ECMAParseError::*;
use TokenType::*;

#[test]
fn parse() {
    let source = format!("  \n	{LF}");
    let mut scanner = Scanner::new(&source);
    let mut tokens = vec![];
    tokens.push(Token::parse_or_rewind(WhiteSpace, &mut scanner, &source).unwrap());
    tokens.push(Token::parse_or_rewind(WhiteSpace, &mut scanner, &source).unwrap());
    tokens.push(Token::parse_or_rewind(LineTerminator, &mut scanner, &source).unwrap());
    tokens.push(Token::parse_or_rewind(WhiteSpace, &mut scanner, &source).unwrap());
    tokens.push(Token::parse_or_rewind(LineTerminator, &mut scanner, &source).unwrap());
    assert_eq!(
        tokens,
        vec![
            Token {
                ttype: WhiteSpace,
                loc: 0,
                children: vec![]
            },
            Token {
                ttype: WhiteSpace,
                loc: 1,
                children: vec![]
            },
            Token {
                ttype: LineTerminator,
                loc: 2,
                children: vec![]
            },
            Token {
                ttype: WhiteSpace,
                loc: 3,
                children: vec![]
            },
            Token {
                ttype: LineTerminator,
                loc: 4,
                children: vec![]
            },
        ]
    )
}

#[test]
fn parse_whitespace() {
    assert_eq!(
        Token::parse_or_rewind(WhiteSpace, &mut Scanner::new(&" "), &" "),
        Ok(Token {
            ttype: WhiteSpace,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(WhiteSpace, &mut Scanner::new(&"	"), &"	"),
        Ok(Token {
            ttype: WhiteSpace,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(WhiteSpace, &mut Scanner::new(&"\u{0009}"), &"\u{0009}"),
        Ok(Token {
            ttype: WhiteSpace,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(WhiteSpace, &mut Scanner::new(&"\u{000B}"), &"\u{000B}"),
        Ok(Token {
            ttype: WhiteSpace,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(WhiteSpace, &mut Scanner::new(&"\u{000C}"), &"\u{000C}"),
        Ok(Token {
            ttype: WhiteSpace,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(WhiteSpace, &mut Scanner::new(&"\u{FEFF}"), &"\u{FEFF}"),
        Ok(Token {
            ttype: WhiteSpace,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(WhiteSpace, &mut Scanner::new(&"a\u{FEFF}"), &"a\u{FEFF}"),
        Err(UnexpectedChar),
    );
    assert_eq!(
        Token::parse_or_rewind(WhiteSpace, &mut Scanner::new(&""), &""),
        Err(UnexpectedEOF),
    );
}

#[test]
fn parse_line_terminator() {
    assert_eq!(
        Token::parse_or_rewind(LineTerminator, &mut Scanner::new(&"\n"), &"\n"),
        Ok(Token {
            ttype: LineTerminator,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(LineTerminator, &mut Scanner::new(&"\u{000A}"), &"\u{000A}"),
        Ok(Token {
            ttype: LineTerminator,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(LineTerminator, &mut Scanner::new(&"\u{000D}"), &"\u{000D}"),
        Ok(Token {
            ttype: LineTerminator,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(LineTerminator, &mut Scanner::new(&"\u{2028}"), &"\u{2028}"),
        Ok(Token {
            ttype: LineTerminator,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(LineTerminator, &mut Scanner::new(&"\u{2029}"), &"\u{2029}"),
        Ok(Token {
            ttype: LineTerminator,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(LineTerminator, &mut Scanner::new(&"a\n"), &"a\n"),
        Err(UnexpectedChar),
    );
    assert_eq!(
        Token::parse_or_rewind(LineTerminator, &mut Scanner::new(&""), &""),
        Err(UnexpectedEOF),
    );
}

#[test]
fn parse_line_terminator_sequence() {
    assert_eq!(
        Token::parse_or_rewind(
            LineTerminatorSequence,
            &mut Scanner::new(&LF.to_string()),
            &LF.to_string()
        ),
        Ok(Token {
            ttype: LineTerminatorSequence,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(
            LineTerminatorSequence,
            &mut Scanner::new(&format!("{CR}a")),
            &format!("{CR}a")
        ),
        Ok(Token {
            ttype: LineTerminatorSequence,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(
            LineTerminatorSequence,
            &mut Scanner::new(&LS.to_string()),
            &LS.to_string()
        ),
        Ok(Token {
            ttype: LineTerminatorSequence,
            loc: 0,
            children: vec![],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(
            LineTerminatorSequence,
            &mut Scanner::new(&PS.to_string()),
            &PS.to_string()
        ),
        Ok(Token {
            ttype: LineTerminatorSequence,
            loc: 0,
            children: vec![],
        })
    );
    let mut sc = Scanner::new(&format!("{CR}{LF}"));
    assert_eq!(
        Token::parse_or_rewind(LineTerminatorSequence, &mut sc, &format!("{CR}{LF}")),
        Ok(Token {
            ttype: LineTerminatorSequence,
            loc: 0,
            children: vec![],
        })
    );
    assert!(sc.is_done());
    let source = "
";
    let mut sc = Scanner::new(source);
    assert_eq!(
        Token::parse_or_rewind(LineTerminatorSequence, &mut sc, source),
        Ok(Token {
            ttype: LineTerminatorSequence,
            loc: 0,
            children: vec![],
        })
    );
    assert!(sc.is_done());
    assert_eq!(
        Token::parse_or_rewind(LineTerminator, &mut Scanner::new(&"a"), &"a"),
        Err(UnexpectedChar),
    );
    assert_eq!(
        Token::parse_or_rewind(LineTerminator, &mut Scanner::new(&""), &""),
        Err(UnexpectedEOF),
    );
}

#[test]
fn parse_single_line_coment_char() {
    assert_eq!(
        Token::parse_or_rewind(SingleLineCommentChar, &mut Scanner::new(&"a"), &"a"),
        Ok(Token {
            ttype: SingleLineCommentChar,
            loc: 0,
            children: vec![Token {
                ttype: SourceCharacter,
                loc: 0,
                children: vec![],
            }],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(SingleLineCommentChar, &mut Scanner::new(&"\n"), &"\n"),
        Err(UnexpectedChar),
    );
    assert_eq!(
        Token::parse_or_rewind(SingleLineCommentChar, &mut Scanner::new(&""), &""),
        Err(UnexpectedEOF),
    );
}

#[test]
fn parse_single_line_coment_chars() {
    assert_eq!(
        Token::parse_or_rewind(
            SingleLineCommentChars,
            &mut Scanner::new(&"aasddads\n"),
            &"aasddads\n"
        ),
        Ok(Token {
            ttype: SingleLineCommentChars,
            loc: 0,
            children: vec![
                Token {
                    ttype: SingleLineCommentChar,
                    loc: 0,
                    children: vec![Token {
                        ttype: SourceCharacter,
                        loc: 0,
                        children: vec![],
                    },],
                },
                Token {
                    ttype: SingleLineCommentChar,
                    loc: 1,
                    children: vec![Token {
                        ttype: SourceCharacter,
                        loc: 1,
                        children: vec![],
                    },],
                },
                Token {
                    ttype: SingleLineCommentChar,
                    loc: 2,
                    children: vec![Token {
                        ttype: SourceCharacter,
                        loc: 2,
                        children: vec![],
                    },],
                },
                Token {
                    ttype: SingleLineCommentChar,
                    loc: 3,
                    children: vec![Token {
                        ttype: SourceCharacter,
                        loc: 3,
                        children: vec![],
                    },],
                },
                Token {
                    ttype: SingleLineCommentChar,
                    loc: 4,
                    children: vec![Token {
                        ttype: SourceCharacter,
                        loc: 4,
                        children: vec![],
                    },],
                },
                Token {
                    ttype: SingleLineCommentChar,
                    loc: 5,
                    children: vec![Token {
                        ttype: SourceCharacter,
                        loc: 5,
                        children: vec![],
                    },],
                },
                Token {
                    ttype: SingleLineCommentChar,
                    loc: 6,
                    children: vec![Token {
                        ttype: SourceCharacter,
                        loc: 6,
                        children: vec![],
                    },],
                },
                Token {
                    ttype: SingleLineCommentChar,
                    loc: 7,
                    children: vec![Token {
                        ttype: SourceCharacter,
                        loc: 7,
                        children: vec![],
                    },],
                },
            ],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(SingleLineCommentChars, &mut Scanner::new(&"\n"), &"\n"),
        Err(UnexpectedChar),
    );
    assert_eq!(
        Token::parse_or_rewind(SingleLineCommentChars, &mut Scanner::new(&""), &""),
        Err(UnexpectedEOF),
    );
}

#[test]
fn parse_single_line_coment() {
    let mut scanner = Scanner::new(&"// aasddads\n");
    let comment = Token::parse_or_rewind(SingleLineComment, &mut scanner, &"// aasddads\n");
    assert!(comment.is_ok());
    let comment = comment.unwrap();
    assert_eq!(comment.loc, 0);
    assert_eq!(comment.ttype, SingleLineComment);
    assert_eq!(scanner.pop(), Some(&'\n'));

    let mut scanner = Scanner::new(&"//");
    let comment = Token::parse_or_rewind(SingleLineComment, &mut scanner, &"//");
    assert!(comment.is_ok());
    let comment = comment.unwrap();
    assert_eq!(comment.loc, 0);
    assert_eq!(comment.ttype, SingleLineComment);
    assert_eq!(scanner.pop(), None);

    let mut scanner = Scanner::new(&"// ");
    let comment = Token::parse_or_rewind(SingleLineComment, &mut scanner, &"// ");
    assert!(comment.is_ok());
    let comment = comment.unwrap();
    assert_eq!(comment.loc, 0);
    assert_eq!(comment.ttype, SingleLineComment);
    assert_eq!(scanner.pop(), None);

    assert_eq!(
        Token::parse_or_rewind(SingleLineComment, &mut Scanner::new(&"\n"), &"\n"),
        Err(UnexpectedChar),
    );
    assert_eq!(
        Token::parse_or_rewind(SingleLineComment, &mut Scanner::new(&""), &""),
        Err(UnexpectedEOF),
    );
}

#[test]
fn parse_multi_line_not_forward_slash_or_asterisk_char() {
    assert_eq!(
        Token::parse_or_rewind(
            MultiLineNotForwardSlashOrAsteriskChar,
            &mut Scanner::new(&"a"),
            &"a"
        ),
        Ok(Token {
            ttype: MultiLineNotForwardSlashOrAsteriskChar,
            loc: 0,
            children: vec![Token {
                ttype: SourceCharacter,
                loc: 0,
                children: vec![],
            }],
        })
    );
    assert_eq!(
        Token::parse_or_rewind(
            MultiLineNotForwardSlashOrAsteriskChar,
            &mut Scanner::new(&"*"),
            &"*"
        ),
        Err(UnexpectedChar),
    );
    assert_eq!(
        Token::parse_or_rewind(
            MultiLineNotForwardSlashOrAsteriskChar,
            &mut Scanner::new(&"/"),
            &"/"
        ),
        Err(UnexpectedChar),
    );
    assert_eq!(
        Token::parse_or_rewind(
            MultiLineNotForwardSlashOrAsteriskChar,
            &mut Scanner::new(&""),
            &""
        ),
        Err(UnexpectedEOF),
    );
}

#[test]
fn parse_multi_line_comment() {
    let _ = Token::parse(MultiLineComment, &mut Scanner::new("/**/"), &"/**/").unwrap();
    let _ = Token::parse(
        MultiLineComment,
        &mut Scanner::new(
            "/* asdasd asd 
		
		*/",
        ),
        &"/* asdasd asd 
		
		*/",
    )
    .unwrap();
    let _ = Token::parse(MultiLineComment, &mut Scanner::new("/*****/"), &"/*****/").unwrap();
}

#[test]
fn parse_single_and_multi_comment() {
    let source = "// This is a comment
/* this is a multi
line comment */";
    let mut scanner = Scanner::new(source);

    let single = Token::parse(SingleLineComment, &mut scanner, &source);
    assert!(single.is_ok());
    let single = single.unwrap();
    assert_eq!(single.loc, 0);
    assert_eq!(single.ttype, SingleLineComment);

    assert!(Token::parse(LineTerminatorSequence, &mut scanner, &source).is_ok());

    let multi = Token::parse(MultiLineComment, &mut scanner, &source);
    assert!(multi.is_ok());
    let multi = multi.unwrap();
    assert_eq!(multi.loc, 21);
    assert_eq!(multi.ttype, MultiLineComment);
}

#[test]
fn parse_private_identifier() {
    let source = "#_";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(PrivateIdentifier, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, PrivateIdentifier);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "#$var";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(PrivateIdentifier, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, PrivateIdentifier);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
}

#[test]
fn parse_identifier_name() {
    let source = "_";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(IdentifierName, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, IdentifierName);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "$var";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(IdentifierName, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, IdentifierName);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 4);
}

#[test]
fn parse_identifier_start() {
    let source = "_";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(IdentifierPart, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, IdentifierPart);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "\\u{00100}";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(IdentifierPart, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, IdentifierPart);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
}

#[test]
fn parse_identifier_start_char() {
    let source = "$";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(IdentifierStartChar, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, IdentifierStartChar);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 0);
    let source = "_";
    let mut scanner = Scanner::new(&source);
    let tok = Token::parse(IdentifierStartChar, &mut scanner, &source).unwrap();
    assert_eq!(tok.ttype, IdentifierStartChar);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 0);
    let source = "a";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(IdentifierStartChar, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, IdentifierStartChar);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
}

#[test]
fn parse_identifier_part() {
    let source = "$";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(IdentifierPart, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, IdentifierPart);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "\\u0000";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(IdentifierPart, &mut scanner, source).unwrap_err();
    assert_eq!(tok, SyntaxError);
}

#[test]
fn parse_identifier_part_char() {
    let source = "$";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(IdentifierPartChar, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, IdentifierPartChar);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 0);
    let source = format!("{ZWJ}");
    let mut scanner = Scanner::new(&source);
    let tok = Token::parse(IdentifierPartChar, &mut scanner, &source).unwrap();
    assert_eq!(tok.ttype, IdentifierPartChar);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 0);
    let source = format!("{ZWNJ}");
    let mut scanner = Scanner::new(&source);
    let tok = Token::parse(IdentifierPartChar, &mut scanner, &source).unwrap();
    assert_eq!(tok.ttype, IdentifierPartChar);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 0);
    let source = "a";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(IdentifierPartChar, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, IdentifierPartChar);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
}

#[test]
fn parse_hex_digit() {
    let source = "0";
    let mut scanner = Scanner::new(source);
    assert_eq!(
        Token::parse(HexDigit, &mut scanner, source),
        Ok(Token {
            ttype: HexDigit,
            loc: 0,
            children: vec!()
        })
    );
    let source = "1";
    let mut scanner = Scanner::new(source);
    assert_eq!(
        Token::parse(HexDigit, &mut scanner, source),
        Ok(Token {
            ttype: HexDigit,
            loc: 0,
            children: vec!()
        })
    );
    let source = "a";
    let mut scanner = Scanner::new(source);
    assert_eq!(
        Token::parse(HexDigit, &mut scanner, source),
        Ok(Token {
            ttype: HexDigit,
            loc: 0,
            children: vec!()
        })
    );
    let source = "C";
    let mut scanner = Scanner::new(source);
    assert_eq!(
        Token::parse(HexDigit, &mut scanner, source),
        Ok(Token {
            ttype: HexDigit,
            loc: 0,
            children: vec!()
        })
    );
}

#[test]
fn parse_hex_4_digits() {
    let source = "0000";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(Hex4Digits, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, Hex4Digits);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 4);
    let source = "1234";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(Hex4Digits, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, Hex4Digits);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 4);
    let source = "aBcD";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(Hex4Digits, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, Hex4Digits);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 4);
}

#[test]
fn mv_hex_4_digits() {
    let source = "0000";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(Hex4Digits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0x0000 as f64);
    let source = "1234";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(Hex4Digits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0x1234 as f64);
    let source = "aBcD";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(Hex4Digits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0xabcd as f64);
}

#[test]
fn parse_hex_digits() {
    let source = "0";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(HexDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, HexDigits);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "12";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(HexDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, HexDigits);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 2);
    let source = "aBcD";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(HexDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, HexDigits);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 4);

    let source = "_0";
    let mut scanner = Scanner::new(source);
    let res = Token::parse(HexDigits_Sep, &mut scanner, source);
    assert_eq!(res, Err(NoHexDigit));
    let source = "1_2";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(HexDigits_Sep, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, HexDigits_Sep);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 3);
    let source = "aB_cD";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(HexDigits_Sep, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, HexDigits_Sep);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 5);
}

#[test]
fn mv_hex_digits() {
    let source = "0";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(HexDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0x0 as f64);
    let source = "12";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(HexDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0x12 as f64);
    let source = "aBcDeeeeee";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(HexDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0xabcdeeeeeei64 as f64);

    let source = "1_2";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(HexDigits_Sep, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0x12 as f64);
    let source = "aB_cD";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(HexDigits_Sep, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0xabcd as f64);
}

#[test]
fn parse_code_point() {
    let source = "0";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(CodePoint, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, CodePoint);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "12";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(CodePoint, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, CodePoint);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "aBcD";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(CodePoint, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, CodePoint);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "10ffff";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(CodePoint, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, CodePoint);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "110000";
    let mut scanner = Scanner::new(source);
    let err = Token::parse(CodePoint, &mut scanner, source).unwrap_err();
    assert_eq!(err, InvalidCodePoint);
}

#[test]
fn parse_unicode_escape_sequence() {
    let source = "u0000";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(UnicodeEscapeSequence, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, UnicodeEscapeSequence);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "u1234";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(UnicodeEscapeSequence, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, UnicodeEscapeSequence);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "uaBcD";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(UnicodeEscapeSequence, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, UnicodeEscapeSequence);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "uaBc";
    let mut scanner = Scanner::new(source);
    let err = Token::parse(UnicodeEscapeSequence, &mut scanner, source).unwrap_err();
    assert_eq!(err, UnexpectedEOF);

    let source = "u{0}";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(UnicodeEscapeSequence, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, UnicodeEscapeSequence);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "u{12}";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(UnicodeEscapeSequence, &mut scanner, source).unwrap();
    assert_eq!(tok.ttype, UnicodeEscapeSequence);
    assert_eq!(tok.loc, 0);
    assert_eq!(tok.children.len(), 1);
    let source = "u{}";
    let mut scanner = Scanner::new(source);
    let err = Token::parse(UnicodeEscapeSequence, &mut scanner, source).unwrap_err();
    assert_eq!(err, NoHexDigit);
    let source = "u{110000}";
    let mut scanner = Scanner::new(source);
    let err = Token::parse(UnicodeEscapeSequence, &mut scanner, source).unwrap_err();
    assert_eq!(err, InvalidCodePoint);
}

#[test]
fn parse_numeric_literal() {
    let source = "10";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, DecimalLiteral);
    assert!(scanner.is_done());
    let source = "010";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, LegacyOctalIntegerLiteral);
    assert!(scanner.is_done());
    let source = "10n";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, DecimalBigIntegerLiteral);
    assert!(scanner.is_done());
    let source = "0b10";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, BinaryIntegerLiteral_Sep);
    assert!(scanner.is_done());
    let source = "0b10n";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, BinaryIntegerLiteral_Sep);
    assert_eq!(tok.children[1].ttype, BigIntLiteralSuffix);
    assert!(scanner.is_done());
    let source = "0B10";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, BinaryIntegerLiteral_Sep);
    assert!(scanner.is_done());
    let source = "0B10n";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, BinaryIntegerLiteral_Sep);
    assert_eq!(tok.children[1].ttype, BigIntLiteralSuffix);
    assert!(scanner.is_done());
    let source = "0o10";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, OctalIntegerLiteral_Sep);
    assert!(scanner.is_done());
    let source = "0o10n";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, OctalIntegerLiteral_Sep);
    assert_eq!(tok.children[1].ttype, BigIntLiteralSuffix);
    assert!(scanner.is_done());
    let source = "0O10";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, OctalIntegerLiteral_Sep);
    assert!(scanner.is_done());
    let source = "0O10n";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, OctalIntegerLiteral_Sep);
    assert_eq!(tok.children[1].ttype, BigIntLiteralSuffix);
    assert!(scanner.is_done());
    let source = "0x10";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, HexIntegerLiteral_Sep);
    assert!(scanner.is_done());
    let source = "0x10n";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, HexIntegerLiteral_Sep);
    assert_eq!(tok.children[1].ttype, BigIntLiteralSuffix);
    assert!(scanner.is_done());
    let source = "0X10";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, HexIntegerLiteral_Sep);
    assert!(scanner.is_done());
    let source = "0X10n";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NumericLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.children[0].ttype, NonDecimalIntegerLiteral_Sep);
    assert_eq!(tok.children[0].children[0].ttype, HexIntegerLiteral_Sep);
    assert_eq!(tok.children[1].ttype, BigIntLiteralSuffix);
}

#[test]
fn identifier_code_points() {
    let source = "name";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(IdentifierName, &mut scanner, source).unwrap();
    assert_eq!(tok.identifier_code_points(source), vec!['n', 'a', 'm', 'e']);
}

#[test]
fn mv_decimal_literal() {
    let source = "10";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(DecimalLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 10.0);
    let source = "1_0.1";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(DecimalLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 10.1);
    let source = "10.1";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(DecimalLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 10.1);
    let source = ".1";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(DecimalLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0.1);
    let source = "0.1";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(DecimalLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0.1);
    let source = "5e-2";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(DecimalLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0.05);
    let source = "10.5e-2";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(DecimalLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0.105);
}

#[test]
fn mv_octal_digits() {
    let source = "10";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(OctalDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 8.0);
    let source = "0";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(OctalDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0.0);
    let source = "32";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(OctalDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0o32 as f64);
}

#[test]
fn mv_binary_digits() {
    let source = "10";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(BinaryDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 2.0);
    let source = "0";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(BinaryDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0.0);
    let source = "011011";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(BinaryDigits, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0b11011 as f64);
}

#[test]
fn mv_legacy_octal_integer_literal() {
    let source = "010";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(LegacyOctalIntegerLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 8.0);
    let source = "00";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(LegacyOctalIntegerLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0.0);
    let source = "032";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(LegacyOctalIntegerLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 0o32 as f64);
}

#[test]
fn mv_non_octal_decimal_integer_literal() {
    let source = "0109";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NonOctalDecimalIntegerLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 109.0);
    let source = "008";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NonOctalDecimalIntegerLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 8.0);
    let source = "039";
    let mut scanner = Scanner::new(source);
    let tok = Token::parse(NonOctalDecimalIntegerLiteral, &mut scanner, source).unwrap();
    assert_eq!(tok.mv(source), 39.0);
}

#[test]
fn parse_string_literal() {
    let source = "\"\"";
    let mut scanner = Scanner::new(source);
    let _tok = Token::parse(StringLiteral, &mut scanner, source).unwrap();
    assert!(scanner.is_done());
    let source = "\"mamma mia\"";
    let mut scanner = Scanner::new(source);
    let _tok = Token::parse(StringLiteral, &mut scanner, source).unwrap();
    assert!(scanner.is_done());
    let source = "\"\\\"\"";
    let mut scanner = Scanner::new(source);
    let _tok = Token::parse(StringLiteral, &mut scanner, source).unwrap();
    assert!(scanner.is_done());
    let source = "\"mamma\\\" mia\"";
    let mut scanner = Scanner::new(source);
    let _tok = Token::parse(StringLiteral, &mut scanner, source).unwrap();
    assert!(scanner.is_done());
    let source = "\"\\\\\"";
    let mut scanner = Scanner::new(source);
    let _tok = Token::parse(StringLiteral, &mut scanner, source).unwrap();
    assert!(scanner.is_done());

    let source = "''";
    let mut scanner = Scanner::new(source);
    let _tok = Token::parse(StringLiteral, &mut scanner, source).unwrap();
    assert!(scanner.is_done());
    let source = "'mamma mia'";
    let mut scanner = Scanner::new(source);
    let _tok = Token::parse(StringLiteral, &mut scanner, source).unwrap();
    assert!(scanner.is_done());
    let source = "'\\''";
    let mut scanner = Scanner::new(source);
    let _tok = Token::parse(StringLiteral, &mut scanner, source).unwrap();
    assert!(scanner.is_done());
    let source = "'mamma\\' mia'";
    let mut scanner = Scanner::new(source);
    let _tok = Token::parse(StringLiteral, &mut scanner, source).unwrap();
    assert!(scanner.is_done());
    let source = "'\\\\'";
    let mut scanner = Scanner::new(source);
    let _tok = Token::parse(StringLiteral, &mut scanner, source).unwrap();
    assert!(scanner.is_done());
}
